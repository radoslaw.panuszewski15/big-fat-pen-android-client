package org.semidude.bigfatpen

import org.junit.Test

import org.junit.Assert.*
import org.semidude.bigfatpen.domain.DRAW_REQUEST
import org.semidude.bigfatpen.paint.drawing.BigPixel
import org.semidude.bigfatpen.paint.drawing.ScribblePart
import java.io.DataOutputStream
import java.io.IOException
import java.io.OutputStream

class NetworkDataEncodingTest {

    @Test
    fun `test encode and decode ScribblePart`() {
        val scribblePart = ScribblePart(
            123,
            listOf(
                BigPixel(1, 2, 3, 4, 5),
                BigPixel(11, 12, 13, 14, 15),
                BigPixel(12, 22, 23, 24, 25)
            ),
            true
        )

        val output = DataOutputStream(object : OutputStream() {

            private val string = StringBuilder()

            override fun write(b: Int) {
                this.string.append(b.toChar())
            }

            override fun toString(): String {
                return this.string.toString()
            }
        })

        output.writeInt(DRAW_REQUEST)
        output.writeInt(scribblePart.pixels.size)

        for (pixel in scribblePart.pixels) {
            output.writeInt(pixel.x)
            output.writeInt(pixel.y)
            output.writeInt(pixel.r)
            output.writeInt(pixel.g)
            output.writeInt(pixel.b)
        }
        output.writeBoolean(scribblePart.isEnd)

        println(output.toString())
    }
}