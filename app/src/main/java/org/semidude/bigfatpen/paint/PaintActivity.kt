package org.semidude.bigfatpen.paint

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import io.reactivex.rxkotlin.subscribeBy
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.activity_paint.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.semidude.bigfatpen.R
import org.semidude.bigfatpen.helpers.networkToUiSchedule
import org.semidude.bigfatpen.helpers.showToast
import java.io.EOFException
import java.net.SocketException
import kotlin.concurrent.thread


class PaintActivity : AppCompatActivity() {

    companion object {
        const val ROOM_ID_ARG = "room_id_arg"
        const val ROOM_NAME_ARG = "room_name_arg"
        const val ROOM_CREATE_ARG = "room_create_arg"
    }

    private val viewModel: PaintViewModel by viewModel()

    private val roomId: Int by lazy { intent.getIntExtra(ROOM_ID_ARG, -1) }
    private val roomName: String by lazy { intent.getStringExtra(ROOM_NAME_ARG) }
    private val createRoom: Boolean by lazy { intent.getBooleanExtra(ROOM_CREATE_ARG, false) }

    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paint)
        setSupportActionBar(toolbar)

        paintView.setOnScribblePartDrawnListener { scribblePart ->
            viewModel.enqueueScribblePartToSend(scribblePart)
        }

        if (createRoom) {
            viewModel.assureConnection()
                .andThen(viewModel.createRoom(roomName))
                .networkToUiSchedule()
                .subscribeBy(
                    onSuccess = { startInfo ->
                        paintView.initialize(startInfo)
                        thread { viewModel.listenToServerMessages() }
                        thread { viewModel.sendScribbleParts() }
                    },
                    onError = {
                        it.printStackTrace()
                        showToast("cannot connect to the server")
                        onBackPressed()
                    }
                )
                .disposeBy(onDestroy)
        }
        else {
            viewModel.assureConnection()
                .andThen(viewModel.joinRoom(roomId))
                .networkToUiSchedule()
                .subscribeBy(
                    onSuccess = { startInfo ->
                        paintView.initialize(startInfo)
                        thread { viewModel.listenToServerMessages() }
                        thread { viewModel.sendScribbleParts() }
                    },
                    onError = {
                        it.printStackTrace()
                        showToast("cannot connect to the server")
                        onBackPressed()
                    }
                )
                .disposeBy(onDestroy)
        }


        viewModel.assureConnection()
            .andThen(viewModel.observeIncomingScribbles())
            .networkToUiSchedule()
            .subscribeBy (
                onNext = { scribblePart -> paintView.updateScribble(scribblePart) },
                onComplete = { onBackPressed() },
                onError = {
                    it.printStackTrace()

                    when (it) {
                        is EOFException -> showToast("server has stopped")
                        is SocketException -> {  }
                        else -> showToast("unknown error occured: ${it.message}")
                    }
                    onBackPressed()
                }
            )
            .disposeBy(onDestroy)

        viewModel.observeIncomingUndoRequests()
            .networkToUiSchedule()
            .subscribeBy(
                onNext = { undoRequest -> paintView.undoLastScribble(undoRequest.scribblerId) }
            )
            .disposeBy(onDestroy)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_paint, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.clear -> {
                paintView.clear()
                true
            }
            R.id.undo -> {
                viewModel.requestUndo(paintView.userId).subscribe() //TODO userId should be hold in viewModel
                true
            }
            R.id.chooseColor -> {
                ColorPickerDialogBuilder
                    .with(this)
                    .setTitle("Choose color")
                    .initialColor(paintView.drawColor)
                    .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                    .density(12)
                    .setPositiveButton("ok") { _, selectedColor, _ ->
                        paintView.drawColor = selectedColor
                        menu.findItem(R.id.chooseColor).icon?.let { changeDrawableColor(it, selectedColor) }
                        menu.findItem(R.id.undo).icon?.let { changeDrawableColor(it, selectedColor) }
                        menu.findItem(R.id.clear).icon?.let { changeDrawableColor(it, selectedColor) }

                    }
                    .setNegativeButton("cancel") { _, _ -> }
                    .build()
                    .show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        viewModel.clear()
        super.onBackPressed()
    }

    private fun changeDrawableColor(drawable: Drawable, selectedColor: Int) {
        drawable.mutate()
        drawable.setColorFilter(selectedColor, PorterDuff.Mode.SRC_ATOP)
    }
}