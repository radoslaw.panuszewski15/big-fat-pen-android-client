package org.semidude.bigfatpen.paint.drawing

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.graphics.Color.*

class ColorRGB(val r: Int, val g: Int, val b: Int)

fun Int.toColorRGB() = ColorRGB(red(this), green(this), blue(this))

class PaintView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {

    //low level stuff
    companion object {
        val DEFAULT_DRAW_COLOR = parseColor("#DA1A5D")
        val DEFAULT_BG_COLOR = WHITE
    }

    private val paint: Paint = Paint()
    var drawColor: Int = DEFAULT_DRAW_COLOR
    var bgColor: Int = DEFAULT_BG_COLOR

    var userId: Int = -1
    private var scribblesHistory = mutableListOf<Scribble>()
    private lateinit var currentScribble: Scribble
    private val lastPixel: BigPixel
        get() = currentScribble.bigPixels.last()

    private var initialized = false
    private lateinit var onScribblePartDrawn: (ScribblePart) -> Unit

    init {
        paint.color = DEFAULT_DRAW_COLOR
        paint.style = Paint.Style.STROKE
    }

    fun initialize(startInfo: StartInfo) {
        this.userId = startInfo.userId
        currentScribble = Scribble.emptyScribble(userId)

        this.scribblesHistory = startInfo.scribbles.toMutableList()
        invalidate()

        initialized = true
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (!initialized) return true

        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                touchStart(x.toInt(), y.toInt())
                invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                touchMove(x.toInt(), y.toInt())
                invalidate()
            }
            MotionEvent.ACTION_UP -> {
                touchUp(x.toInt(), y.toInt())
                invalidate()
            }
        }
        return true
    }

    fun clear() {
        bgColor = DEFAULT_BG_COLOR
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {

        if (!initialized) return

        canvas.save()
        canvas.drawColor(bgColor)

        for (scribble in scribblesHistory) {
            canvas.drawPoints(scribble.pixelsRaw, paint.apply { color = scribble.color })
        }
        canvas.drawPoints(currentScribble.pixelsRaw, paint.apply { color = currentScribble.color })

        canvas.restore()
    }

    fun updateScribble(scribblePart: ScribblePart) {
        val correspondingScribble: Scribble? = scribblesHistory.findLast { it.scribblerId == scribblePart.scribblerId }

        if (correspondingScribble != null && !correspondingScribble.isCompleted) {
            correspondingScribble.addMultipleBigPixels(scribblePart.pixels)
            correspondingScribble.isCompleted = scribblePart.isEnd
        }
        else scribblesHistory.add(scribblePart.toScribble())
        invalidate()
    }

    fun undoLastScribble(scribblerId: Int) {
        val lastScribble = scribblesHistory.findLast { it.scribblerId == scribblerId }
        scribblesHistory.remove(lastScribble)
        invalidate()
    }

    fun setOnScribblePartDrawnListener(onScribblePartDrawn: (ScribblePart) -> Unit) {
        this.onScribblePartDrawn = onScribblePartDrawn
    }

    private fun touchStart(x: Int, y: Int) {
        currentScribble.addBigPixel(
            captureBigPixel(x, y)
                .also { pixel ->
                    onScribblePartDrawn(
                        ScribblePart(
                            1,
                            listOf(pixel),
                            isEnd = false
                        )
                    )
                }
        )
    }

    private fun touchMove(x: Int, y: Int) {
        currentScribble.addMultipleBigPixels(
            LineBuilder.buildLine(
                lastPixel.x,
                lastPixel.y,
                x,
                y,
                drawColor.toColorRGB()
            )
                .also { pixels ->
                    onScribblePartDrawn(ScribblePart(1, pixels, isEnd = false))
                }
        )
    }

    private fun touchUp(x: Int, y: Int) {
        currentScribble.addMultipleBigPixels(
            LineBuilder.buildLine(
                lastPixel.x,
                lastPixel.y,
                x,
                y,
                drawColor.toColorRGB()
            )
                .also { pixels ->
                    onScribblePartDrawn(ScribblePart(1, pixels, isEnd = true))
                }
        )
        currentScribble.isCompleted = true

        scribblesHistory.add(currentScribble)
        currentScribble = Scribble.emptyScribble(userId)
    }

    private fun captureBigPixel(x: Int, y: Int): BigPixel {
        val xBeginning = x - (x % 5)
        val yBeginning = y - (y % 5)
        val color = drawColor.toColorRGB()

        return BigPixel(xBeginning, yBeginning, color.r, color.g, color.b)
    }
}