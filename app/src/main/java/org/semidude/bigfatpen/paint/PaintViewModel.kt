package org.semidude.bigfatpen.paint

import android.util.Log
import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.semidude.bigfatpen.helpers.buildList
import org.semidude.bigfatpen.helpers.makeList
import org.semidude.bigfatpen.paint.drawing.*
import java.lang.Exception
import java.net.SocketException
import java.util.concurrent.LinkedBlockingQueue
import kotlin.system.measureTimeMillis
import org.semidude.bigfatpen.domain.*

class PaintViewModel(
    private val networkManager: NetworkManager
): ViewModel() {

    private lateinit var networkGate: NetworkGate
    private val scribblePartsToSend = LinkedBlockingQueue<ScribblePart>()
    private val scribblePartsSubject = BehaviorSubject.create<ScribblePart>()
    private val undoRequestsSubject = BehaviorSubject.create<UndoRequest>()

    fun assureConnection() =
        networkManager.assureConnection()
            .doOnSuccess { networkGate = it }
            .flatMapCompletable { Completable.complete() }

    fun joinRoom(roomId: Int) =
        Single.fromCallable<StartInfo> {
            networkGate.writeInt(JOIN_ROOM_REQUEST)
            networkGate.writeInt(roomId)
            readStartInfo()
        }

    fun createRoom(roomName: String) =
        Single.fromCallable<StartInfo> {
            networkGate.writeInt(CREATE_ROOM_REQUEST)
            networkGate.writeInt(roomName.length)
            networkGate.writeBytes(roomName)
            readStartInfo()
        }

    fun observeIncomingScribbles(): Observable<ScribblePart> = scribblePartsSubject

    fun observeIncomingUndoRequests(): Observable<UndoRequest> = undoRequestsSubject

    fun listenToServerMessages() {
        try {
            while (networkManager.isConnected) {
                val messageCode = networkGate.readInt()

                when (messageCode) {
                    DRAW_REQUEST -> scribblePartsSubject.onNext(readScribblePart())
                    UNDO_REQUEST -> undoRequestsSubject.onNext(readUndoRequest())
                }
            }
            scribblePartsSubject.onComplete()
        }
        catch (e: Exception) {
            scribblePartsSubject.onError(e)
        }
    }

    private fun readUndoRequest(): UndoRequest {
        val scribblerId = networkGate.readInt()
        return UndoRequest(scribblerId)
    }

    private fun readStartInfo(): StartInfo {
        val joinCode = networkGate.readInt()
        val userId = networkGate.readInt()
        val scribbleParts = readMultipleScribbleParts()
        return StartInfo(userId, scribbleParts.map { it.toScribble() })
    }

    private fun readMultipleScribbleParts(): List<ScribblePart> {
        val scribblesCount = networkGate.readInt()

        return makeList { scribbles ->
            for (i in 0 until scribblesCount) {
                scribbles += readScribblePart()
            }
        }
    }

    private fun readScribblePart(): ScribblePart {
        val pixels = mutableListOf<BigPixel>()
        val pixelsCount = networkGate.readInt()
        val scribblerId = networkGate.readInt()

        for (i in 0 until pixelsCount) {
            pixels += BigPixel(
                x = networkGate.readInt(),
                y = networkGate.readInt(),
                r = networkGate.readInt(),
                g = networkGate.readInt(),
                b = networkGate.readInt()
            )
        }
        val isEnd = networkGate.readBoolean()
        return ScribblePart(scribblerId, pixels, isEnd)
    }

    fun enqueueScribblePartToSend(scribblePart: ScribblePart) {
        scribblePartsToSend.put(scribblePart)
    }

    fun sendScribbleParts() {
        try {
            while (!Thread.interrupted()) {
                val scribblePart = buildList<ScribblePart> {
                        do {
                            add(scribblePartsToSend.take())
                        } while (scribblePartsToSend.size > 0)
                    }.mergeScribbleParts()

                Log.d("koles", scribblePart.pixels.size.toString())

                networkGate.writeInt(DRAW_REQUEST)
                networkGate.writeInt(scribblePart.pixels.size)

                for (pixel in scribblePart.pixels) {
                    networkGate.writeInt(pixel.x)
                    networkGate.writeInt(pixel.y)
                    networkGate.writeInt(pixel.r)
                    networkGate.writeInt(pixel.g)
                    networkGate.writeInt(pixel.b)
                }
                networkGate.writeBoolean(scribblePart.isEnd)
            }
        }
        catch (e: InterruptedException) {
            //just end the loop
        }
        catch (e: SocketException) {
            //just end the loop
        }
    }

    fun requestUndo(scribblerId: Int) =
        Completable.fromAction {
            networkGate.writeInt(UNDO_REQUEST)
        }
            .subscribeOn(Schedulers.io())

    override fun onCleared() {
        clear()
    }

    fun clear() {
        networkManager.close()
    }
}