package org.semidude.bigfatpen.paint.drawing

import org.semidude.bigfatpen.helpers.buildList

data class BigPixel(
    var x: Int,
    var y: Int,
    var r: Int,
    var g: Int,
    var b: Int
)

data class Scribble(
    val scribblerId: Int,
    val bigPixels: MutableList<BigPixel>,
    var isCompleted: Boolean
) {
    companion object {
        fun emptyScribble(scribblerId: Int) =
            Scribble(scribblerId, mutableListOf(), false)
    }

    val pixelsRaw: FloatArray
        get() = bigPixels
            .flatMap { bigPixel ->
                buildList<Float> {
                    for (i in 0 until 5) {
                        for (j in 0 until 5) {
                            add(bigPixel.x.toFloat() + i)
                            add(bigPixel.y.toFloat() + j)
                        }
                    }
                }
            }.toFloatArray()

    val color: Int
        get() = bigPixels.firstOrNull()?.let {
            rgb(
                it.r / 255f,
                it.g / 255f,
                it.b / 255f
            )
        }
            ?: rgb(0f, 0f, 0f)

    fun addBigPixel(pixel: BigPixel) {
        bigPixels += pixel
    }

    fun addMultipleBigPixels(pixels: List<BigPixel>) {
        bigPixels.addAll(pixels)
    }
}

data class ScribblePart(
    val scribblerId: Int,
    val pixels: List<BigPixel>,
    val isEnd: Boolean
)

fun ScribblePart.toScribble() =
    Scribble(scribblerId, pixels.toMutableList(), isEnd)

fun List<ScribblePart>.mergeScribbleParts(): ScribblePart {
    if (!all { it.scribblerId == first().scribblerId })
        throw IllegalAccessException("All scribble parts to be merged should have the same scribblerId")


    return ScribblePart(first().scribblerId, flatMap { it.pixels }, last().isEnd)
}

class StartInfo(
    val userId: Int,
    val scribbles: List<Scribble>
)

fun rgb(red: Float, green: Float, blue: Float): Int =
    -0x1000000 or
    ((red * 255.0f + 0.5f).toInt() shl 16) or
    ((green * 255.0f + 0.5f).toInt() shl 8) or
    (blue * 255.0f + 0.5f).toInt()