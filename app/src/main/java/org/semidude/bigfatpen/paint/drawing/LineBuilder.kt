package org.semidude.bigfatpen.paint.drawing

import org.semidude.bigfatpen.helpers.buildList

object LineBuilder {

    private lateinit var color: ColorRGB

//    fun buildLine(beginX: Int, beginY: Int, endX: Int, endY: Int, colorInt: Int) =
//        buildLine(beginX, endX, beginY, endY, ColorRGB(red(colorInt), green(colorInt), blue(colorInt)))


    fun buildLine(beginX: Int, beginY: Int, endX: Int, endY: Int, color: ColorRGB): List<BigPixel> {
        LineBuilder.color = color
        val deltaX = endX - beginX
        val deltaY = endY - beginY

        return when {
            deltaX == 0 -> buildStraightLine(
                beginY,
                endY,
                beginX,
                true
            )
            deltaY == 0 -> buildStraightLine(
                beginX,
                endX,
                beginY,
                false
            )
            Math.abs(deltaX) > Math.abs(deltaY) -> buildSlopingLine(
                beginX,
                endX,
                beginY,
                deltaX,
                deltaY,
                true
            )
            else -> buildSlopingLine(
                beginY,
                endY,
                beginX,
                deltaY,
                deltaX,
                false
            )
        }
    }

    private fun buildStraightLine(begin: Int, end: Int, constPoint: Int, vertical: Boolean): List<BigPixel> {
        var begin = begin
        val sign = if (end - begin > 0) 1 else -1
        return buildList {
            while (begin != end) {
                begin += sign
                add(choosePoint(begin, constPoint, vertical))
            }
        }
    }

    private fun buildSlopingLine(beginX: Int, endX: Int, beginY: Int, deltaX: Int, deltaY: Int, yCloser: Boolean): List<BigPixel> {
        val signY = if (deltaY > 0) 1 else -1
        val signX = if (endX - beginX > 0) 1 else -1
        val deltaErr = Math.abs(deltaY.toDouble() / deltaX)
        var error = 0.0
        var y = beginY
        var x = beginX + 1
        return buildList {
            while (x != endX) {
                error += deltaErr
                if (error >= 0.5) {
                    y += signY
                    error -= 1.0
                }
                add(choosePoint(y, x, yCloser))
                x += signX
            }
        }
    }

    private fun choosePoint(begin: Int, constPoint: Int, vertical: Boolean): BigPixel {
        return if (vertical)
            BigPixel(
                constPoint,
                begin,
                color.r,
                color.g,
                color.b
            )
        else
            BigPixel(
                begin,
                constPoint,
                color.r,
                color.g,
                color.b
            )
    }
}