package org.semidude.bigfatpen.di

import android.app.Application
import org.koin.android.ext.android.startKoin
import org.semidude.bigfatpen.di.appModule

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule))
    }
}