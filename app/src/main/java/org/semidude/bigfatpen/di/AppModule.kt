@file:Suppress("RemoveExplicitTypeArguments")

package org.semidude.bigfatpen.di

import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.semidude.bigfatpen.domain.NetworkManager
import org.semidude.bigfatpen.join.JoinViewModel
import org.semidude.bigfatpen.paint.PaintViewModel

val appModule = module {

    single { NetworkManager() }

    viewModel<PaintViewModel> { PaintViewModel(get()) }

    viewModel<JoinViewModel> { JoinViewModel(get()) }
}