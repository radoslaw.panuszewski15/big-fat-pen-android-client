package org.semidude.bigfatpen.domain

data class Room(
    val roomId: Int,
    val roomName: String
)