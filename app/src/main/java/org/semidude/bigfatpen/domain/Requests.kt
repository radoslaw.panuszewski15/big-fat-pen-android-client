package org.semidude.bigfatpen.domain

const val LOGIN_REQUEST = 0
const val DRAW_REQUEST = 1
const val UNDO_REQUEST = 2
const val CREATE_ROOM_REQUEST = 3
const val JOIN_ROOM_REQUEST = 4

data class UndoRequest(
    val scribblerId: Int
)