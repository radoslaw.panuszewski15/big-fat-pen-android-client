package org.semidude.bigfatpen.domain

import io.reactivex.Single
import java.io.DataInput
import java.io.DataInputStream
import java.io.DataOutput
import java.io.DataOutputStream
import java.lang.IllegalStateException
import java.net.Socket
import java.util.concurrent.locks.ReentrantLock

class NetworkManager {

    private lateinit var socket: Socket
    internal lateinit var inputStream: DataInputStream
    internal lateinit var outputStream: DataOutputStream
    var isConnected = false

    fun connect() =
        Single.fromCallable<NetworkGate> {
            socket = Socket("10.0.2.2", 1234)
            inputStream = DataInputStream(socket.getInputStream())
            outputStream = DataOutputStream(socket.getOutputStream())
            isConnected = true
            NetworkGate(this).also {
                networkGate = it
//                connectedCondition.signalAll()
            }
        }

    private lateinit var networkGate: NetworkGate
    private val lock = ReentrantLock()
    private val connectedCondition = lock.newCondition()

    fun assureConnection() =
        Single.fromCallable<NetworkGate> {
            if (!isConnected)
//                connectedCondition.await()
                throw IllegalStateException("not connected")

            networkGate
        }

    fun close() {
        if (isConnected) {
            socket.close()
            isConnected = false
        }
    }
}

class NetworkGate(private val networkManager: NetworkManager)
    : DataInput by networkManager.inputStream,
      DataOutput by networkManager.outputStream