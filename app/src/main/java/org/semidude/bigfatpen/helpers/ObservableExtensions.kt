package org.semidude.bigfatpen.helpers

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T> Observable<T>.networkToUiSchedule() =
    this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.networkToUiSchedule() =
    this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())