package org.semidude.bigfatpen.helpers

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast

fun inflateView(context: Context, resource: Int, parent: ViewGroup, attachToRoot: Boolean = false) =
    LayoutInflater.from(context)
        .inflate(resource, parent, attachToRoot)

val Context.preferences: SharedPreferences
    get() = PreferenceManager.getDefaultSharedPreferences(this)

fun Context.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

inline fun <reified T> Context.startActivity() {
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T> Context.startActivity(extraInitializer: Intent.() -> Unit) {
    startActivity(Intent(this, T::class.java).apply(extraInitializer))
}