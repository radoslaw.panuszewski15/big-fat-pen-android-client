package org.semidude.bigfatpen.join

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_join.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.semidude.bigfatpen.R
import org.semidude.bigfatpen.helpers.networkToUiSchedule
import org.semidude.bigfatpen.helpers.showToast
import org.semidude.bigfatpen.helpers.startActivity
import org.semidude.bigfatpen.paint.PaintActivity

class JoinActivity : AppCompatActivity() {

    private val viewModel: JoinViewModel by viewModel()
    private val roomsAdapter = RoomsAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join)

        roomsRecycler.apply {
            adapter = roomsAdapter.apply {
                onItemClickListener = { room, viewModel ->
                    roomsAdapter.selectedRoom = room
                    roomsAdapter.notifyDataSetChanged()
                    nextButton.text = "JOIN ROOM"
                    nextButton.isEnabled = true
                    nextButton.setBackgroundResource(R.color.colorAccent)
                }
            }
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }

        createRoomEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (createRoomEditText.text.toString().isNotBlank()) {
                    nextButton.text = "CREATE ROOM"
                    nextButton.isEnabled = true
                    nextButton.setBackgroundResource(R.color.colorAccent)
                }
            }
        })

        nextButton.setOnClickListener {
            if (!viewModel.isConnected) {
                viewModel.connectToServer()
                    .networkToUiSchedule()
                    .subscribeBy(
                        onSuccess = { rooms ->
                            roomsAdapter.updateItems(rooms)

                            createRoomEditText.visibility = View.VISIBLE

                            nextButton.text = "SELECT ROOM TO CONTINUE"
                            nextButton.isEnabled = false
                            nextButton.setBackgroundResource(R.color.colorPrimaryDark)
                        },
                        onError = {
                            showToast("cannot connect to the server")
                        }
                    )
            } else {
                startActivity<PaintActivity> {
                    if (createRoomEditText.text.toString().isNotBlank()) {
                        putExtra(PaintActivity.ROOM_NAME_ARG, createRoomEditText.text.toString())
                        putExtra(PaintActivity.ROOM_CREATE_ARG, true)
                    } else {
                        putExtra(PaintActivity.ROOM_ID_ARG, roomsAdapter.selectedRoom?.roomId)
                        putExtra(PaintActivity.ROOM_CREATE_ARG, false)
                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        createRoomEditText.setText("")

        if (!viewModel.isConnected) {
            nextButton.text = "CONNECT TO SERVER"
            nextButton.isEnabled = true
            nextButton.setBackgroundResource(R.color.colorAccent)
            roomsAdapter.updateItems(emptyList())
            roomsAdapter.selectedRoom = null
            createRoomEditText.visibility = View.GONE
        }
        else if (roomsAdapter.selectedRoom == null) {
            nextButton.text = "SELECT ROOM TO CONTINUE"
            nextButton.isEnabled = false
            nextButton.setBackgroundResource(R.color.colorPrimaryDark)
        }
        else {
            nextButton.text = "JOIN!"
            nextButton.isEnabled = true
            nextButton.setBackgroundResource(R.color.colorAccent)
        }
    }
}