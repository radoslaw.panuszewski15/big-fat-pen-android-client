package org.semidude.bigfatpen.join

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.room_item.view.*
import org.semidude.bigfatpen.R
import org.semidude.bigfatpen.domain.Room
import org.semidude.bigfatpen.helpers.BaseAdapter
import org.semidude.bigfatpen.helpers.inflateView

class RoomsAdapter(context: Context) : BaseAdapter<RoomsAdapter.RoomViewHolder, Room>(context) {

    var selectedRoom: Room? = null

    override fun doCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        val itemView = inflateView(context, R.layout.room_item, parent, false)
        return RoomViewHolder(itemView)
    }

    override fun doBindViewHolder(viewHolder: RoomViewHolder, position: Int) {
        viewHolder.bindRoom(items[position])
    }

    inner class RoomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val roomNameView: TextView = itemView.roomNameView
        val iconView: ImageView = itemView.iconView

        fun bindRoom(room: Room) {
            roomNameView.text = room.roomName

            if (selectedRoom == room) {
                itemView.setBackgroundResource(R.color.colorAccent)
                roomNameView.setTextColor(context.getColor(android.R.color.white))
                iconView.setImageResource(R.drawable.home_white)
            }
            else {
                itemView.setBackgroundResource(android.R.color.white)
                roomNameView.setTextColor(context.getColor(android.R.color.black))
                iconView.setImageResource(R.drawable.home)
            }
        }
    }
}