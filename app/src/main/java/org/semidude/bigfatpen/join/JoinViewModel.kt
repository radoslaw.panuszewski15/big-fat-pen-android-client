package org.semidude.bigfatpen.join

import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.semidude.bigfatpen.domain.NetworkGate
import org.semidude.bigfatpen.domain.NetworkManager
import org.semidude.bigfatpen.domain.Room
import org.semidude.bigfatpen.helpers.makeList

class JoinViewModel(
    private val networkManager: NetworkManager
) : ViewModel() {

    private lateinit var networkGate: NetworkGate

    val isConnected get() = networkManager.isConnected

    fun connectToServer(): Single<List<Room>> =
        networkManager.connect()
            .subscribeOn(Schedulers.io())
            .doOnSuccess { networkGate = it }
            .flatMap {
                Single.fromCallable { readAvailableRooms() }
            }


    private fun readAvailableRooms(): List<Room> {
        val loginCode = networkGate.readInt()
        val roomsCount = networkGate.readInt()

        return makeList{ rooms ->
            for (i in 0 until roomsCount) {

                val roomId = networkGate.readInt()
                val nameLength = networkGate.readInt()

                val roomName = buildString {
                    for (j in 0 until nameLength) {
                        append(networkGate.readByte().toChar())
                    }
                }
                rooms += Room(roomId, roomName)
            }
        }
    }

    override fun onCleared() {
        networkManager.close()
    }
}